---
layout: default
title: About
nav_order: 99
description: "Meta info about the project and this site"
permalink: /about
---

The content of the site is the resources needed for a class in IoT using [particle.io](https://www.particle.io/)

## Technical

The site is a [jekyll static site](https://jekyllrb.com/) hosted on [gitlab.com](https://gitlab.com/) and uses [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) for deployment.

The repo is public and is available [here](https://gitlab.com/eal-itt/iot-with-particle). Feel free to create issues or submit merge requests.

The theme we use is [just the docs](https://pmarsceill.github.io/just-the-docs/). The link also serves as a reference for the UI candy we get from tha theme.


## License

Just the Docs is distributed by an [MIT license](https://github.com/pmarsceill/just-the-docs/tree/master/LICENSE.txt).

![cc 4.0 SA](assets/images/cc.png) The rest of the site is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
