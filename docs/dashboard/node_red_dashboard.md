---
layout: page
title: node-red dashboard
parent: Dashboard
description: "node-red-dashboard"
nav_order: 3
permalink: /dashboard/node_red_dashboard
---

# Description

This guide describes how to set up the <a href="https://flows.nodered.org/node/node-red-dashboard" target="_blank">node-red-dashboard</a> palette. 
This requires an instance of <a href="https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_setup" target="_blank">node-red running</a> on a RPi and a working <a href="https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_boron_serial" target="_blank">serial connection</a> between node-red and Boron.

The node-red-dashboard palette gives you access to dashboard widgets like gauges, graphs, buttons, sliders etc. that can be used to build custom dashboards.
It also serves the dashboard through `http://<your_pi_ip>:1880/ui/`
 
## node-red dashboard palette - install guide

1. Watch this <a href="https://youtu.be/X8ustpkAJ-U" target="_blank">video</a> to get an overview and instructions for installation
2. After installation ensure that your dashboard is running by going to `http://<your_pi_ip>:1880/ui/`
3. Read the documentation to get familiar with the different widgets

## node-red dashboard with Boron serial data

<a href="https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_boron_serial" target="_blank">Previous</a> your learned how to establish serial communication between node-red and Boron.  
Building on that and your knowledge about node-red-dashboard, setup a simple dashboard with the following functionality:

1. Detect button presses from the <a href="https://eal-itt.gitlab.io/iot-with-particle/electronics/led_btn_module" target="_blank">led and button module</a> 
2. control the LED's on the <a href="https://eal-itt.gitlab.io/iot-with-particle/electronics/led_btn_module" target="_blank">led and button module</a>
3. Read ADC value in 1 sec. intervals and display it in a gauge. This includes notification if the current is below 4mA or over 20mA.

All values from the Boron must be transfered through serial to the node-red-dashboard using your <a href="https://eal-itt.gitlab.io/iot-with-particle/docs/communication/two-way-serial/" target="_blank">serial protocol</a>  

**Remember** you need to handle the protocol in the Boron. Making a flowchart as well as documenting you protocol is required before starting the implementation.  
To parse the serial data from the Boron, you can use the function node.  
Read about it and how to write functions in node-red <a href="https://nodered.org/docs/user-guide/writing-functions" target="_blank">here</a>. 

Below is an example of a node-red flow using the function module, to parse comma seperated serial values send from the Boron as:

```
Serial1.printlnf(String(voltage) + "," + String(btn_msg));
```

<img src="../assets/images/node_red_parse_flow.png" width="500" alt="node red parse flow">

The *parse_current* function node uses this code  

```
var input = msg.payload.split(",");
var current = input[0];
msg = {payload:current};

return msg;
```

The *parse_btn* function node uses this code  

```
var input = msg.payload.split(",");
var button = parseInt(input[1]);
if (button === 1){
    msg = {payload:"Button 1 off"};    
}
else if (button === 0){
    msg = {payload:"Button 1 on"};    
}

return msg;
```

The dashboard from above flow with button deactivated

<img src="../assets/images/node_red_dash_flow_1.png" width="500" alt="node red dash flow button on">

The dashboard from above flow with button activated

<img src="../assets/images/node_red_dash_flow_2.png" width="500" alt="node red dash flow button off"> 

