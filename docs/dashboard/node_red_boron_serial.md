---
layout: page
title: node-red Boron serial
parent: Dashboard
description: "Serial between node-red and Boron"
nav_order: 2
permalink: /dashboard/node_red_boron_serial
---

# Description

This guide describes how to set up two-way serial UART communication between the Boron and node-red running on a raspberry pi.
This requires an instance of <a href="https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_setup" target="_blank">node-red running</a> on a RPi and a working <a href="https://eal-itt.gitlab.io/iot-with-particle/docs/communication/serial/" target="_blank">serial connection</a> between RPi and Boron.
 
## Serial communication between node-red and Boron

1. Flash this program to the Boron

    ```
    /*
     * Project node-red serial_1way
    * Description: serial between Boron and node-red
    * Author: NISI
    * Date: 2020-02-12
    */

    const unsigned long UPDATE_PERIOD_MS_1 = 1000; //1mS period
    unsigned long lastUpdate;
    unsigned long counter; 

    void setup() {
    // Put initialization like pinMode and begin functions here.
    Serial1.begin(9600);
    }

    void loop() {
    // Send data in intervals
      if (millis() - lastUpdate >= UPDATE_PERIOD_MS_1) {
    		lastUpdate = millis();
        Serial1.printlnf("Hello from Boron %d", ++counter);
      }
    }
    ``` 
    The program sends a message on Boron `TX` every 1 second

2. Add a `serial in` node from node-red and double click it to configure

    <img src="../assets/images/node_red_serial_in.png" width="400" alt="serial in node config"> 

3. Connect a debug node to the output of the serial node and deploy (notice serial node says connected)

    <img src="../assets/images/node_red_debug_node.png" width="350" alt="connected debug node"> 

4. in the debug window you should now see the messages sent from the Boron

    <img src="../assets/images/node_red_debug_window.png" width="400" alt="serial data in debug window"> 

5. To test serial from node-red to Boron add a serial out node and an inject node. Connect the inject node out to serial out in

    <img src="../assets/images/node_red_serial_out.png" width="400" alt="node red serial out">

6. Edit the inject node to send a string

    <img src="../assets/images/node_red_inject_config.png" width="400" alt="inject node config">

7. Flash this code to the Boron

    ```
    /*
    * Project serial_2way
    * Description: serial between Boron, node-red and putty
    * Author: NISI
    * Date: 2020-02-12
    */

    const unsigned long UPDATE_PERIOD_MS = 1000; //1mS period
    unsigned long lastUpdate;
    unsigned long counter; 

    void setup() {
    // Put initialization like pinMode and begin functions here.  
        Serial1.begin(9600);
    }

    void loop() {
    // Send data in intervals
        if (millis() - lastUpdate >= UPDATE_PERIOD_MS) {
                lastUpdate = millis();
            Serial1.printlnf("sending test data %d", ++counter);
        }

        // if serial data received on Boron RX pin, print to serial (USB)
        while (Serial1.available() > 0) {
            String incoming = Serial1.readStringUntil('\n');
            Serial.printlnf("received from node-red: " + incoming);  
        }
    }
    ```
    The code monitors if serial data on `serial1` is available, reads the data and sends it to `serial`

8. Open a serial connection to the Boron with a terminal eg. Putty
9. Click the inject node to send serial data to the Boron

    <img src="../assets/images/node_red_inject_serial.png" width="400" alt="inject serial to Boron">

10. Confirm that the message is received in the terminal

    <img src="../assets/images/node_red_putty_serial.png" width="300" alt="putty monitor serial from Boron">