---
layout: page
title: Dashboard
nav_order: 3
has_children: true
permalink: /dashboard/
---

# Dashboard

A <a href="https://en.wikipedia.org/wiki/Dashboard_(business)" target="_blank">dashboard</a> is useful to both, developers and users. 

## External resources

<a href="https://nodered.org/" target="_blank">node-red</a>