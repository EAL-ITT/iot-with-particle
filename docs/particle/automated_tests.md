---
layout: page
title: Automated testing
parent: Particle
description: "Automated cpp code testing"
nav_order: 2
---

# Description

We have create a template project to enable static code checks of particle projects.

The repo is [here](https://gitlab.com/moozer/particle-check-template)

# Usage

Basic usage

1. Fork the [project](https://gitlab.com/moozer/particle-check-template)
2. Check that pipeline runs in your forked project
3. Look into [`.gitlab-ci.yml`](https://gitlab.com/moozer/particle-check-template/-/blob/master/.gitlab-ci.yml) to see how the parts combine and read the [`readme.md`](https://gitlab.com/moozer/particle-check-template/-/blob/master/readme.md) file for further hints.

    * `.gitlab-ci.yml`: the CI configuration file
    * `setup.sh`: installs the needed packages
    * `check_ino.sh`: the script that goes through the tests

4. Copy files to your own project
5. `Commit` and `push`, and see the pipeline run.
