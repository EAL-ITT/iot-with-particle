---
layout: page
title: Particle
nav_order: 5
has_children: true
permalink: /particle/
---

# Particle related stuff

In here you can find resources and guides related to the particle.io development boards and framework



## External resources

<a href="https://docs.particle.io/" target="_blank">Particle docs</a>  
<a href="https://docs.particle.io/tutorials/device-os/device-os/" target="_blank">Particle tutorials</a>  
<a href="https://docs.particle.io/datasheets/cellular/boron-datasheet/" target="_blank">Boron Datasheet</a>  
<a href="https://docs.particle.io/reference/device-os/firmware/boron/" target="_blank">Boron Reference</a>  
<a href="https://docs.particle.io/tutorials/developer-tools/workbench/#particle-libraries" target="_blank">Particle workbench libraries</a>  
<a href="https://infocenter.nordicsemi.com/pdf/nRF52840_PS_v1.0.pdf" target="_blank">nRF52840 datasheet</a>  
