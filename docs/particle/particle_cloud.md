---
layout: page
title: Particle cloud
parent: Particle
description: "particle cloud guide"
nav_order: 5
permalink: /particle/particle_cloud
---

# Description

This page describes how to publish data to the particle device cloud.
The particle device cloud is capable of retrieving data from a particle device in different ways. Data can be published in intervals or by request with particel variables. Commands can be sent to a particle device to control peripherals using particle function.

The particle cloud is protected with Tokens to prevent unauthorized access to your data.

The below information is a supplement to the official <a href="https://docs.particle.io/reference/device-os/firmware/boron/#cloud-functions" target="_blank">particle cloud reference</a>

## Particle publish

<a href="https://docs.particle.io/reference/device-os/firmware/boron/#particle-publish- " target="_blank">Documentation</a>

1. Make a small program that sends messages to the particle device cloud

```
int counter = 0;

void setup() {

}

void loop() {
  Particle.publish("counter_event", String(counter), 60, PRIVATE);
  delay(5000);
  counter++;
}
```

2. create an api token using the <a href="https://docs.particle.io/reference/developer-tools/cli/#particle-token-create" target="_blank">particle CLI</a>    
<img src="../assets/images/token_cli.png" width="600" alt="token cli">

3. store your token in a safe place

4. test that you can receive your published event using curl:
`curl -H "authorization: Bearer <your token here>" \ https://api.particle.io/v1/events/<your device id here>`

Message received should resemble this:

```
event: UCL_BORON_2
data: {"data":"51","ttl":60,"published_at":"2020-03-07T18:44:11.094Z","coreid":"e00fce68de993b22a74276d9"}
```

## Read from node-red

Your published data can be read from node-red using the <a href="https://flows.nodered.org/node/node-red-contrib-particle" target="_blank">particle node-red palette</a>

1. install the <a href="https://flows.nodered.org/node/node-red-contrib-particle" target="_blank">particle node-red palette</a>
2. From the particle palette add an `SSE` node  
<img src="../assets/images/SSE.png" width="100" alt="SSE node">  
3. Configure the SSE node cloud field with your token and preferred name  
<img src="../assets/images/particle_cloud_config.png" width="500" alt="cloud config">  
4. Configure the subscribe and event fields to match your device and event name  
<img src="../assets/images/cloud_subscribe.png" width="500" alt="cloud subscribe">  
5. Test it by connecting a debug node to the output  
<img src="../assets/images/SSE_connect.png" width="500" alt="SSE connect">  
The output should resemble:  
```
{"data":"59","ttl":60,"published_at":"2020-03-07T19:00:42.898Z","coreid":"e00fce68de993b22a74276d9","name":"counter_event"}
```

## Particle variable

<a href="https://docs.particle.io/reference/device-os/firmware/boron/#particle-variable-" target="_blank">Documentation</a>

A particle variable is a special variable that can be triggered from a remote API call, ie. from your dash board.
This is useful for requesting data from the Boron instead of sending data to the cloud continuously.

1. Add a <a href="https://docs.particle.io/reference/device-os/firmware/boron/#particle-variable-" target="_blank">variable</a> to a boron sketch 

```
SYSTEM_THREAD(ENABLED);
int counter = 0;
double current = 0;

void setup() {
  Particle.variable("curr", current);
}

void loop() {
  current = counter;
  counter++;
  delay(200);
}
```

2. Test it with curl  
`curl "https://api.particle.io/v1/devices/<your device id>/curr?access_token=<your access token>"`  
the response should resemble this:  
```{"cmd":"VarReturn","name":"curr","result":2302,"coreInfo":{"last_app":"","last_heard":"2020-03-07T20:24:07.279Z","connected":true,"last_handshake_at":"2020-03-07T20:16:49.226Z","deviceID":"e00fce68de993b22a74276d9","product_id":13}}```

## Trigger variable from node-red

The variables can be triggered from node-red using the variable node from the Particle palette

1. add a variable node from the Particle palette  
<img src="../assets/images/Particle_variable.png" width="100" alt="particle variable node">
2. Configure the node  
<img src="../assets/images/variable_config.png" width="400" alt="variable config">
3. add an inject node and configure it  
<img src="../assets/images/variable_inject.png" width="400" alt="variable inject">
4. Add a debug node to see the data returned from the variable, the finished flow should look like this:  
<img src="../assets/images/variable_flow.png" width="500" alt="variable flow">
5. Click the inject node to request data, the debug output should show the variable response  
<img src="../assets/images/variable_payload.png" width="300" alt="variable payload">