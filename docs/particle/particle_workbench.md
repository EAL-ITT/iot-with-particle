---
layout: page
title: Particle workbench setup
parent: Particle
description: "Particle workbench setup guide"
nav_order: 7
permalink: /particle/particle_workbench
---

# Description

This page is a guide on how to flash a particle device locally using workbench for Visual Studio Code.  
Workbench is an IDE addon for Visual Studio Code.  
 
This is the preferred development method in contrast to the web IDE which requires data plan ($$) usage 

Most of the setup is described in the guide at <a href="https://docs.particle.io/quickstart/workbench/" target="_blank">https://docs.particle.io/quickstart/workbench/</a>

The below is provided as a compliment to that guide.

### Workbench setup/usage (IDE based on Visual Studio Code)

1. Follow the <a href="https://docs.particle.io/quickstart/workbench/" target="_blank">workbench quickstart guide</a> If you already have Visual studio code installed scroll down to: <a href="https://docs.particle.io/quickstart/workbench/#vs-code-already-installed" target="_blank">VS Code already installed</a>
2. Create a new project
3. Configure project for device
4. The device OS is probably not the latest version, update device os to latest version before using local flash, more <a href="https://docs.particle.io/tutorials/device-os/device-os/#managing-device-os" target="_blank">here</a>  
```I only had success updating the device OS locally (in the guide scroll to 'Updating locally') and it took a while, when the system led on the board dims up and down in a cyan color the OS is updated, you can confirm this from the particle console. Current OS when writing this is 1.4.4```

5. Copy the blink an led sketch to the .ino sketch
6. Change the two delay commands on line 60 and 67 to delay(2000)
7. Use ctrl-shift+p to open command palette and type particle compile application (local)
8. from command palette type particle flash application (local)  
when the system led on the board dims up and down in a cyan color the firmware is updated
9. If you succeed in flashing the blink sketch to the Boron, you should see the blue led next to the usb plug flashing in 1 sec intervals
