---
layout: page
title: Use CPP files
parent: Particle
description: "Use .cpp files, not .ino"
nav_order: 8
---

# Description

The difference between .ino files and .cpp files are very litte, so in order to conform to standards use .cpp. This will enable to use all the regular .cpp tools available.


# Converting

The automated way requires the particle cli program, se [here](https://docs.particle.io/tutorials/developer-tools/cli/).

```bash
particle preprocess <INOFILE> -save-to <CPPFILE>
```

This is used in [the template project](https://gitlab.com/moozer/particle-check-template/-/blob/master/check_ino.sh).


The manual way is to add

```cpp
#include "application.h"
```

at the top of the source files, and perhaps some more. See [particle preprocess](https://github.com/particle-iot/wiring-preprocessor).
