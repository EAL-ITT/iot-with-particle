---
layout: page
title: Automated documentation
parent: Particle
description: "Automated doxygen documentation generation"
nav_order: 1
---

# Description

The repo that includes the static code check also includes generating documentation.

The repo is [here](https://gitlab.com/moozer/particle-check-template).

# Usage

Start at setting up the [static code check](https://eal-itt.gitlab.io/iot-with-particle/docs/particle/automated_tests/).

If you have done this previously, you may want to pull updates from the gitlab repo to you forked repo.

Basic usage

1. Check that pipeline runs in your forked project
2. Go to the gitlab pages associated with your project. You may find the link under `settings->pages`.
3. See the result and compare with the cpp code

  Note that there might be a 20 min delay between the first successful deployment of pages and the pages being available.

4. Implement doxygen generated documentation in your own project.
