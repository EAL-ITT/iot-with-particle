---
layout: page
title: 4-20mA current loop tester
parent: Electronics
description: "4-20mA current loop tester"
nav_order: 1
permalink: /electronics/current_loop_tester
---

# Description

In order to simulate sensors that uses the 4-20mA standard it is necessary to build an electronic device that can output current between 0 and 20mA.
4-20mA test devices are used in industrial environments to debug industrial systems.  
The device described here is not an industrial grade device and is only intended for development.

Nordcad has a <a href="https://www.nordcad.eu/student-forum/" target="_blank">Student section</a>
 that aims to get you started using Orcad, it contains a great <a href="https://www.nordcad.eu/student-forum/beginners-course/simulation/" target="_blank">tutorial</a>
 on Pspice simulation.

Simulating with OrCad Pspice is taught in the electronics course.

## 4-20mA current loop tester build guide

Follow the guide at <a href="https://circuitdigest.com/electronic-circuits/4-20ma-current-loop-tester-using-op-amp-as-voltage-to-current-converter" target="_blank">https://circuitdigest.com/electronic-circuits/4-20ma-current-loop-tester-using-op-amp-as-voltage-to-current-converter</a>

1. Before ordering components and building the circuit, simulate it using OrCad Pspice. Especially the 0-20mA range adjustment is important to confirm.  
This article explains how to sweep eg. a potentiometer (R1) in OrCad capture. (Update: article currently offline was at https://m.eet.com/media/1180248/)
See OrCad schematic and simulation results below.
2. Decide on components based on what's in current stock and component cost if ordering is needed.
3. Order components if needed
4. Build the circuit on breadboard and test it according to simulation results
4. Build the circuit on veroboard
    1. ic's should always be in sockets  
    <img src="../assets/images/8pin_DIL_socket.png" width="50" alt="8pin DIL socket">  
    2. Make sure to use pin headers or similar for all inputs and outputs (5V, current out, GND)  
    3. The goal is to make a standalone module.
5. Measure the circuit to confirm the simulation results
6. Document the build with:
    1. Schematic
    2. BOM (bill of material)
    3. Simulation results
    4. Measurement results
    5. Pictures of the build
    5. User guide
7. Before interfacing to eg. an <a href="https://infocenter.nordicsemi.com/pdf/nRF52840_PS_v1.0.pdf#%5B%7B%22num%22%3A4028%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C85.039%2C463.423%2Cnull%5D" target="_blank">ADC</a>
 you need to build a module that converts the current to voltage and scales that voltage to the ADC range  

Schematic drawn in OrCad:  
<img src="../assets/images/0-20mA_schematic.png" width="500" alt="0-20mA current simulation">

Simulation result:  
<img src="../assets/images/0-20mA_i_sim.png" width="600" alt="0-20mA current simulation">
