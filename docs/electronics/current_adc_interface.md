---
layout: page
title: 4-20mA current to voltage interface
parent: Electronics
description: "current to voltage interface"
nav_order: 2
permalink: /electronics/current_adc_interface
---

# Description

The output of a 4-20mA sensor is in nature a current. 
To measure this current with an analog to digital converter (ADC) on a micro controller, the current needs to converted to a voltage.  

An ADC has a voltage range between 0 (usually GND) and a reference voltage. The voltage range depends on the specific ADC's reference voltage, which will be the maximum voltage that the ADC can read.

The Boron development platform is using a microcontroller named nRF52840 and from the <a href="https://infocenter.nordicsemi.com/pdf/nRF52840_PS_v1.0.pdf#%5B%7B%22num%22%3A4028%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C85.039%2C463.423%2Cnull%5D" target="_blank">nRF52840 datasheet</a> you can look up the reference ADC voltage

## 4-20mA current to voltage interface build guide

1. Simulate the *current loop tester with interface connected* circuit in Orcad Pspice.
2. Calculate R16 using ohms law:  
*R=Umax/Imax*  
(Umax = desired voltage output, Imax = maximum current throught the resistor)  
3. Modify the current loop tester according to the *Modified current loop tester schematic*
4. Build the *interface* circuit on breadboard
5. Connect the modified current loop tester with the interface and test it according to your Pspice simulation results
6. Build the interface module on veroboard 
    1. R14 represents the input resistance of the Boron ADC and should be replaced by a pin header for voltage out
    1. ic's should always be in sockets  
    <img src="../assets/images/8pin_DIL_socket.png" width="50" alt="8pin DIL socket">  
    2. Make sure to use pin headers or similar for all inputs and outputs (5V, GND, current in, voltage out)  
    3. The goal is to make a standalone interface module that is connected between the current loop tester and the Boron ADC input. 

*Modified current loop tester schematic*  
<img src="../assets/images/0-20mA_w_output.png" width="500" alt="Modified current loop tester schematic"> 

*Interface schematic*  
<img src="../assets/images/0-20mA_iface_only.png" width="350" alt="Interface schematic"> 

*Current loop tester with interface connected*  
<img src="../assets/images/0-20mA_w_iface_schematic.png" width="700" alt="Current loop tester with interface connected"> 

*Interface output simulation*  
<img src="../assets/images/0-20mA_iface_voltage_sim.png" width="600" alt="Interface output simulation"> 