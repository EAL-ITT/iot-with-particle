---
layout: page
title: Led and button module
parent: Electronics
description: "generic module with 2 LED's and 2 tact switches"
nav_order: 3
permalink: /electronics/led_btn_module
---

<a href="" target="_blank"></a>

# Description

Having LED's and buttons attached to your embedded system is always useful for both testing and user functionality.
This guide shows how to build a simple module on vero board, the module has two LED's and two buttons but can easily be expanded if needed. 

Components used:

| Designator    | Component     |
|:------------- |:------------- |
| J1            | [pin header](https://www.cypax.dk/vare/14.071.4010) |
| D1, D2        | [LED 3mm or 5mm](https://www.cypax.dk/vare/00.040.3705.01) |
| R1, R2        | [resistor 1/4W 5%](https://www.cypax.dk/produkter/-/category/vis/Kulfilm5%2514W) |
| SW1, SW2      | [DTS62 tact switch](https://www.cypax.dk/vare/10.010.6200) |

## Led and button module build guide

1. Design and draw a veroboard version of the below schematic.   
    Make the board size as small as possible.  
    Use black wire for ground, red for power and different colors for the rest.   
    <img src="../assets/images/led_btn_schematic.png" width="400" alt="led_btn schematic">  
2. Calculate the resistor size assuming a 3.3V source and maximum 10 mA through each led. 
    **important** The Boron GPIO pins can each supply a maximum of 14mA, if exceeded they might suffer damage!
3. Cut a piece of stripboard, size according to your design.
4. Find all of the components including wire.   
    Use solid core wire, not multi core
5. Place components without soldering, make a mark where you need to cut traces   
6. Cut traces as necessary.
7. Solder your components  
    Use the microscope to check for solder bridges
8. Test both led’s by connecting common ground. Then 3.3V to each led. If successful, your led will light up.
9.	Test both buttons with a multimeter connected between common ground and the connector pin attached to the button. If successful, the multimeter will show close to 0 ohm when the button is pushed.
6. Document the build with:
    1. Schematic
    2. BOM (bill of material)
    4. Measurement results
    5. Pictures of the build
    5. User guide
