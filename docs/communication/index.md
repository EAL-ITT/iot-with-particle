---
layout: page
title: Communication
nav_order: 2
has_children: true
permalink: /communication/
---

# Communication

This section contains information and links related to how devices exchange information.
