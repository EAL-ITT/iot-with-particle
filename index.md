---
layout: page
title: Home
nav_order: 1
description: "Project companion site"
permalink: /
---

# IoT with particle.io

This site contains resources like links, diagrams, guides and so on to be used in a course at IT technology at [UCL](https://ucl.dk).

![system overview](https://gitlab.com/EAL-ITT/20s-itt2-project/raw/master/docs/photos/project_overview_no_mesh.png "system overview")
